#include "types.h"
#include "stat.h"
#include "user.h"

void
chrono(char *argv)
{
	int prev = uptime();
	int seg = atoi(argv);
	int n = 1;
	while(n < seg + 1){
		if(uptime() - prev >= 180){ //Aproximadamente 1 seg.
			prev = uptime();
			printf(1, "%d\n", n);
			n++;
		}
	}
}

int
main(int argc, char **argv)
{
	if(argc == 2) chrono(argv[1]);
	else printf(1, "fail, i need a element!\n");
	

  	exit();
}
