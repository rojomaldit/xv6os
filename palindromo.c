#include "types.h"
#include "stat.h"
#include "user.h"

int
palindromo(char *argv, int length){
	int r = 0, i = 0, j = length - 1;
	while(i <= j && argv[i] == argv[j]){
		i++;
		j--;
	}
	if(i >= j) r = 1;

	return r;
}

uint pot(uint a){
	for(int i = 0; i < 30; i++){
		a = a * 2;
	}
	return a;
}

int
main(int argc, char **argv)
{
	if(argc == 2){
		int len = strlen(argv[1]);
		printf(1, palindromo(argv[1], len) ? "Si es palindroma \n" : "No es palindroma \n");
	}
	else{
		printf(1, "I need a word\n");
	}

	exit();
}
